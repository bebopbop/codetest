package com.android.fyber.objects;

/**
 * Created by manyeon on 11/6/15.
 */
public class FyberItem  {
    private String mTitle;
    private String mTeaser;
    private String mLowres;
    private String mHires;
    private int mPayout;

    public void setTitle(String pTitle) {
        mTitle = pTitle;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTeaser(String pTeaser) {
        mTeaser = pTeaser;
    }

    public String getTeaser() {
        return mTeaser;
    }

    public void setLowres(String pLowres) {
        mLowres = pLowres;
    }

    public String getLowres() {
        return mLowres;
    }

    public void setHires(String pHires) {
        mHires = pHires;
    }

    public String getHires() {
        return mHires;
    }

    public void setPayout(int pPayout) {
        mPayout = pPayout;
    }

    public int getPayout() {
        return mPayout;
    }
}
