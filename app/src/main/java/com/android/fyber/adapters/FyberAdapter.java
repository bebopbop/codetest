package com.android.fyber.adapters;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.fyber.FyberApplication;
import com.android.fyber.R;
import com.android.fyber.objects.FyberItem;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;

import java.util.List;

/**
 * Created by manyeon on 11/6/15.
 */
public class FyberAdapter extends RecyclerView.Adapter<FyberAdapter.FyberViewHolder> {

    private ImageLoader mImageLoader;
    private List<FyberItem> mItems;
    private FyberApplication mApp;

    public FyberAdapter(List<FyberItem> pItems) {
        mApp = FyberApplication.getApplication();
        mImageLoader = mApp.getImageLoader();
        mItems = pItems;
    }

    @Override
    public FyberViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fyber_item, viewGroup, false);
        FyberViewHolder viewHolder = new FyberViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final FyberViewHolder fyberViewHolder, int i) {
        FyberItem item = mItems.get(i);
        fyberViewHolder.titleText.setText(item.getTitle());
        fyberViewHolder.teaserText.setText(item.getTeaser());
        fyberViewHolder.payoutText.setText(String.format(mApp.getResources().getString(R.string.payout), item.getPayout()));

        mImageLoader.get(item.getHires(), new ImageLoader.ImageListener() {

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                Bitmap bm = response.getBitmap();
                if(bm != null) {
                    fyberViewHolder.hiresImage.setImageBitmap(bm);
                } else {
                    fyberViewHolder.hiresImage.setImageBitmap(BitmapFactory.decodeResource(mApp.getResources(), R.mipmap.ic_launcher));
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                fyberViewHolder.hiresImage.setImageBitmap(BitmapFactory.decodeResource(mApp.getResources(), R.mipmap.ic_launcher));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    class FyberViewHolder extends RecyclerView.ViewHolder {

        private ImageView hiresImage;
        private TextView titleText;
        private TextView teaserText;
        private TextView payoutText;

        public FyberViewHolder(View itemView) {
            super(itemView);

            hiresImage = (ImageView)itemView.findViewById(R.id.hires);
            titleText = (TextView)itemView.findViewById(R.id.title);
            teaserText = (TextView)itemView.findViewById(R.id.teaser);
            payoutText = (TextView)itemView.findViewById(R.id.payout);
        }
    }
}
