package com.android.fyber.objecttype;

import com.android.fyber.objects.FyberItem;
import com.android.fyber.utils.DevLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manyeon on 11/5/15.
 */
public class FyberObject extends AbstractObject {
    private String TAG_OFFERS = "offers";
    private String TAG_TITLE = "title";
    private String TAG_TEASER = "teaser";
    private String TAG_THUMBNANIL = "thumbnail";
    private String TAG_LOWRES = "lowres";
    private String TAG_HIRES = "hires";
    private String TAG_PAYOUT = "payout";

    private List<FyberItem> mFyberItems;

    public FyberObject() {
        mFyberItems = new ArrayList<FyberItem>();
    }

    @Override
    public boolean onResponseListener(String pResponse) {
        try {
            JSONObject json = new JSONObject(pResponse);
            mCode = json.getString(TAG_CODE);
            mMessage = json.getString(TAG_MESSAGE);

            if(mCode.equals("OK") == false) {
                return false;
            }

            JSONArray offerList = json.getJSONArray(TAG_OFFERS);

            for(int i = 0; i < offerList.length(); ++i) {

                FyberItem fyberItem = new FyberItem();

                JSONObject obj = offerList.getJSONObject(i);

                try {
                    String title = obj.getString(TAG_TITLE);
                    fyberItem.setTitle(title);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    String teaser = obj.getString(TAG_TEASER);
                    fyberItem.setTeaser(teaser);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    int payout = obj.getInt(TAG_PAYOUT);
                    fyberItem.setPayout(payout);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    String lowres = obj.getJSONObject(TAG_THUMBNANIL).getString(TAG_LOWRES);
                    fyberItem.setLowres(lowres);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    String hires = obj.getJSONObject(TAG_THUMBNANIL).getString(TAG_HIRES);
                    fyberItem.setHires(hires);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                mFyberItems.add(fyberItem);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return true;
    }

    public List<FyberItem> getFyberItems() {
        return mFyberItems;
    }
}
