package com.android.fyber.objecttype;

/**
 * Created by manyeon on 11/5/15.
 */
public abstract class AbstractObject {
    protected String OK = "OK";
    protected String TAG_CODE = "code";
    protected String TAG_MESSAGE = "message";

    protected String mCode;
    protected String mMessage;

    public abstract boolean onResponseListener(String pResponse);

    public String getCode() {
        return mCode;
    }

    public String getMessage() {
        return mMessage;
    }
}
