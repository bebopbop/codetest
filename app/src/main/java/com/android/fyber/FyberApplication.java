package com.android.fyber;

import android.app.Application;

import com.android.fyber.managers.NetworkManager;
import com.android.fyber.managers.SettingManager;
import com.android.fyber.managers.UtilManager;
import com.android.fyber.objects.AdvertisingIdClient;
import com.android.fyber.utils.LruBitmapCache;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

import java.util.Map;

/**
 * Created by manyeon on 11/5/15.
 */
public class FyberApplication extends Application {

    private static final String TAG = "FyberApplication";

    private static final String SIGNATURE = "X-Sponsorpay-Response-Signature";

    private static FyberApplication mApp;
    private boolean mStarted;
    private boolean mCheckSignature;

    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    private volatile NetworkManager mNetworkManager = null;
    private volatile UtilManager mUtilManager = null;
    private volatile SettingManager mSettingManager = null;

    @Override
    public void onCreate() {
        super.onCreate();

        FyberApplication.mApp = this;
        mStarted = false;

        onStart();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    private void onStart() {
        if(mStarted == false) {
            mStarted = true;
            getNetworkManager();
            getUtilManager();
            getSettingManager();
            getAdvertiseInformation();
        }
    }

    public static FyberApplication getApplication() {
        return mApp;
    }

    public boolean getCheckSignature() {
        return mCheckSignature;
    }

    public RequestQueue getRequestQueue() {
        if(mRequestQueue == null)
            mRequestQueue = Volley.newRequestQueue(mApp);

        return mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();

        if(mImageLoader == null)
            mImageLoader = new ImageLoader(mRequestQueue, new LruBitmapCache());

        return mImageLoader;
    }

    public NetworkManager getNetworkManager() {
        if(mNetworkManager == null) {
            synchronized (this) {
                if(mNetworkManager == null) {
                    mNetworkManager = new NetworkManager();
                }
            }
        }

        return mNetworkManager;
    }

    public UtilManager getUtilManager() {
        if(mUtilManager == null) {
            synchronized (this) {
                if(mUtilManager == null) {
                    mUtilManager = new UtilManager(mApp);
                }
            }
        }

        return mUtilManager;
    }

    public SettingManager getSettingManager() {
        if(mSettingManager == null) {
            synchronized (this) {
                if(mSettingManager == null) {
                    mSettingManager = new SettingManager(mApp);
                }
            }
        }

        return mSettingManager;
    }

    public void getAdvertiseInformation() {
        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    AdvertisingIdClient.AdInfo adInfo = AdvertisingIdClient.getAdvertisingIdInfo(mApp);
                    boolean enabled = adInfo.isLimitAdTrackingEnabled();
                    mSettingManager.setAdvertId(adInfo.getId());
                    mSettingManager.setAdvertLimitedTracking(enabled);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }).start();
    }

    public void checkSignature(Map<String, String> headers) {

        if(headers.containsKey(SIGNATURE)) {
            String signature = headers.get(SIGNATURE);

            if(signature == mSettingManager.getSha1()) {
                mCheckSignature = true;
            } else {
                mCheckSignature = false;
            }

        }
    }

}
