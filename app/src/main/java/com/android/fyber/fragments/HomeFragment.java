package com.android.fyber.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.fyber.FyberApplication;
import com.android.fyber.R;
import com.android.fyber.managers.NetworkManager;

/**
 * Created by manyeon on 11/6/15.
 */
public class HomeFragment extends Fragment {


    private NetworkManager mNetworkManager;

    private View mView;
    private RadioGroup mRadioAppID, mRadioUID, mRadioPub, mRadioLocale;
    private Button mRequestAPIBtn;
    private TextView mErrorMessage;

    private int checkedAppID, checkedUID, checkedPub, checkedLocale;

    private OnEventListener mListener;

    public interface OnEventListener {
        public void onEvent(String params);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnEventListener)activity;
        } catch (ClassCastException e) {
            throw new ClassCastException();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        mView = view;

        initManager();
        initView(view);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mErrorMessage.setVisibility(View.GONE);
    }

    private void initManager() {
        FyberApplication app = FyberApplication.getApplication();
        mNetworkManager = app.getNetworkManager();
    }

    private void initView(View view) {
        mErrorMessage = (TextView)view.findViewById(R.id.errorMessage);

        mRadioAppID = (RadioGroup)view.findViewById(R.id.radioAppID);
        mRadioUID = (RadioGroup)view.findViewById(R.id.radioUID);
        mRadioPub = (RadioGroup)view.findViewById(R.id.radioPub);
        mRadioLocale = (RadioGroup)view.findViewById(R.id.radioLocale);

        mRequestAPIBtn = (Button)view.findViewById(R.id.requestButton);
        mRequestAPIBtn.setOnClickListener(onRequestListener);

        checkedAppID = mRadioAppID.getCheckedRadioButtonId();
        checkedUID = mRadioUID.getCheckedRadioButtonId();
        checkedPub = mRadioPub.getCheckedRadioButtonId();
        checkedLocale = mRadioLocale.getCheckedRadioButtonId();

        mRadioAppID.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                checkedAppID = checkedId;
            }
        });

        mRadioUID.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                checkedUID = checkedId;
            }

        });

        mRadioPub.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                checkedPub = checkedId;
            }

        });

        mRadioLocale.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                checkedLocale = checkedId;
            }

        });
    }

    View.OnClickListener onRequestListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if(mNetworkManager.isNetworkAvaiable(getActivity())) {
                String appId = ((RadioButton)mView.findViewById(mRadioAppID.getCheckedRadioButtonId())).getText().toString();
                String uid = ((RadioButton)mView.findViewById(mRadioUID.getCheckedRadioButtonId())).getText().toString();
                String pub = ((RadioButton)mView.findViewById(mRadioPub.getCheckedRadioButtonId())).getText().toString();
                String locale = ((RadioButton)mView.findViewById(mRadioLocale.getCheckedRadioButtonId())).getText().toString();
                StringBuilder sb = new StringBuilder();
                sb.append(appId).append(",").append(locale).append(",").append(pub).append(",").append(uid);
                mListener.onEvent(sb.toString());
            } else {
                mErrorMessage.setVisibility(View.VISIBLE);
                mErrorMessage.setText(R.string.no_network_connection);
            }
        }
    };

}
