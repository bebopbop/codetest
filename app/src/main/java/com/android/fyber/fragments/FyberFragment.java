package com.android.fyber.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.fyber.Constants;
import com.android.fyber.FyberApplication;
import com.android.fyber.R;
import com.android.fyber.adapters.FyberAdapter;
import com.android.fyber.managers.NetworkManager;
import com.android.fyber.managers.SettingManager;
import com.android.fyber.managers.UtilManager;
import com.android.fyber.objects.FyberItem;
import com.android.fyber.objecttype.FyberObject;
import com.android.fyber.utils.DevLog;

import java.security.NoSuchAlgorithmException;
import java.util.List;

/**
 * Created by manyeon on 11/6/15.
 */
public class FyberFragment extends Fragment {

    private FyberApplication mApp;
    private UtilManager mUtilManager;
    private SettingManager mSettingManager;
    private NetworkManager mNetworkManager;

    private LinearLayout mLoadContainer;
    private RecyclerView mRecyclerView;
    private TextView mCheckSignature;
    private TextView mNoOffer;

    private List<FyberItem> mFyberItems;

    private String mParam;


    public void setParam(String pParam) {
        mParam = pParam;

        DevLog.defaultLogging(mParam);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fyberlist, container, false);
        initManager();
        initView(view);
        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    private void initManager() {
        mApp = FyberApplication.getApplication();
        mNetworkManager = mApp.getNetworkManager();
        mSettingManager = mApp.getSettingManager();
        mUtilManager = mApp.getUtilManager();
    }

    private void initView(View view) {
        mLoadContainer = (LinearLayout)view.findViewById(R.id.loadContainer);
        mCheckSignature = (TextView)view.findViewById(R.id.checkSignature);
        mNoOffer = (TextView)view.findViewById(R.id.noOffer);
        mRecyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        setFyberData();
    }

    private String setResultString() {
        String[] param = mParam.split(",");

        long timeStamp = System.currentTimeMillis() / 1000;
        String advertId = mSettingManager.getAdvertId();
        boolean enabled = mSettingManager.getAdvertLimitedTracking();
        String ip = mUtilManager.getIPAddress();

        StringBuilder sb = new StringBuilder();
        sb.append("appid=").append(param[0]).append("&device_id=").append(advertId).append("&google_ad_id_limited_tracking_enabled=").append(enabled)
                .append("&ip=").append(ip).append("&locale=").append(param[1]).append("&offer_types=112").append("&pub0=").append(param[2]).append("&timestamp=").append(timeStamp)
                .append("&uid=").append(param[3]);

        return sb.toString();
    }

    private void setFyberData() {

        loadView(true);

        String value = setResultString();
        String resultString = new StringBuilder().append(value).append("&1c915e3b5d42d05136185030892fbb846c278927").toString();

        try {
            mSettingManager.setSha1(mUtilManager.sha1(resultString));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String url = new StringBuilder().append("/feed/v1/offers.json?").append(value).append("&hashkey=").append(mSettingManager.getSha1()).toString();

        mNetworkManager.requestGET(url, new NetworkManager.OnNetworkResponseListener() {

            @Override
            public void onNetworkResponse(String pResponse) {
                DevLog.defaultLogging("Response: " + pResponse);

                loadView(false);

                if (pResponse.equals(Constants.NETWORK_ERROR)) {
                    setNoOffer(R.string.response_error);
                    return;
                }

                checkSignature();
                FyberObject object = new FyberObject();
                boolean result = object.onResponseListener(pResponse);

                if (result == false) {
                    DevLog.defaultLogging(object.getMessage());
                    return;
                }

                mFyberItems = object.getFyberItems();

                if (mFyberItems.size() == 0) {
                    setNoOffer(R.string.no_offers);
                    return;
                }

                mRecyclerView.setAdapter(new FyberAdapter(mFyberItems));
            }

        });
    }

    private void loadView(boolean pIsLoad) {
        if(pIsLoad) {
            mLoadContainer.setVisibility(View.VISIBLE);
        } else {
            mLoadContainer.setVisibility(View.GONE);
        }
    }

    private void checkSignature() {
        if(mApp.getCheckSignature()) {
            mCheckSignature.setTextColor(getResources().getColor(android.R.color.holo_blue_dark));
            mCheckSignature.setText(R.string.real_response);

        } else {
            mCheckSignature.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
            mCheckSignature.setText(R.string.fake_response);
        }
    }

    private void setNoOffer(int pRes) {
        mNoOffer.setVisibility(View.VISIBLE);
        mNoOffer.setText(pRes);
    }

}
