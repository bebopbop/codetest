package com.android.fyber.activities;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.android.fyber.Constants;
import com.android.fyber.R;
import com.android.fyber.fragments.HomeFragment;

public class HomeActivity extends AppCompatActivity implements HomeFragment.OnEventListener {

    private Context mContext;
    public HomeFragment mFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;
        mFragment = new HomeFragment();
        setFragment(mFragment);
    }


    public void setFragment(Fragment pFragment) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.containerFrame, pFragment);
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public void onEvent(String params) {
        Intent intent = new Intent(mContext, FyberActivity.class);
        intent.putExtra(Constants.KEY_PARAM_VALUE, params);
        startActivity(intent);
    }

}
