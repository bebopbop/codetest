package com.android.fyber.activities;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.android.fyber.Constants;
import com.android.fyber.R;
import com.android.fyber.fragments.FyberFragment;

public class FyberActivity extends AppCompatActivity {

    private Context mContext;
    public FyberFragment mFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String param = getIntent().getStringExtra(Constants.KEY_PARAM_VALUE);
        mContext = this;
        mFragment = new FyberFragment();
        mFragment.setParam(param);
        setFragment(mFragment);
    }


    private void setFragment(Fragment pFragment) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.containerFrame, pFragment);
        ft.addToBackStack(null);
        ft.commit();
    }


}
