package com.android.fyber.managers;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.android.fyber.Constants;
import com.android.fyber.FyberApplication;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by manyeon on 11/5/15.
 */
public class NetworkManager {
    private static final String TAG = "NetworkManager";
    private static final String SERVER_URL = "http://api.fyber.com";

    private static final int SOCKET_TIMEOUT = 3000;

    private FyberApplication mApp;
    private RequestQueue mRequestQueue;

    public interface OnNetworkResponseListener {
        void onNetworkResponse(String pResponse);
    }

    public NetworkManager() {
        mApp = FyberApplication.getApplication();
        mRequestQueue = mApp.getRequestQueue();
        mRequestQueue.start();
    }

    public boolean isNetworkAvaiable(Context pContext) {
        try{
            ConnectivityManager connManager = (ConnectivityManager)pContext.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo.State wifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState();	// Wifi
            if(wifi == NetworkInfo.State.CONNECTED) {
                return true;
            }

            NetworkInfo.State mobile = connManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState();	// mobile
            if(mobile == NetworkInfo.State.CONNECTED) {
                return true;
            }
        } catch(NullPointerException e) {
            return false;
        }

        return false;
    }

    public void requestGET(String pPath, final OnNetworkResponseListener pNetworkResponse) {
        String url = SERVER_URL + pPath;

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                if(pNetworkResponse != null) {
                    pNetworkResponse.onNetworkResponse(response);
                }
            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if(pNetworkResponse != null) {
                    pNetworkResponse.onNetworkResponse(Constants.NETWORK_ERROR);
                }
            }

        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return super.getParams();
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                mApp.checkSignature(response.headers);
                return super.parseNetworkResponse(response);
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = super.getHeaders();

                if(headers == null || headers.equals(Collections.emptyMap())) {
                    headers = new HashMap<String, String>();
                }

                return headers;
            }

        };

        request.setRetryPolicy(new DefaultRetryPolicy(SOCKET_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        mRequestQueue.add(request);
    }

}
