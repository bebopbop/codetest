package com.android.fyber.managers;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.text.format.Formatter;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

/**
 * Created by manyeon on 11/5/15.
 */
public class UtilManager {
    private Context mContext;

    public UtilManager(Context pContext) {
        mContext = pContext;
    }

    public String getAppVersion() {

        try {
            PackageInfo packageInfo = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), PackageManager.GET_META_DATA);
            return packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getModel() {
        StringBuilder sb = new StringBuilder();
        sb.append(Build.MANUFACTURER).append(" ").append(Build.MODEL);
        return sb.toString();
    }

    public String getDeviceId() {
        TelephonyManager telephonyManager = (TelephonyManager)mContext.getSystemService(Context.TELEPHONY_SERVICE);
        String id = null;

        if(telephonyManager != null) {
            id = telephonyManager.getDeviceId();
        }

        return (TextUtils.isEmpty(id) ? "N/A" : id);
    }

    public String getCurrentLanguage() {
        return Locale.getDefault().getLanguage();
    }

    public String getAndroidVersion() {
        return Build.VERSION.RELEASE;
    }

    public String getIPAddress() {
        WifiManager wifiManager = (WifiManager)mContext.getSystemService(Context.WIFI_SERVICE);
        String ip = Formatter.formatIpAddress(wifiManager.getConnectionInfo().getIpAddress());
        if(ip == null || ip.equals("0.0.0.0")) {
            ip = "109.235.143.113";
        }
        return ip;
    }

    public String sha1(String input) throws NoSuchAlgorithmException {
        MessageDigest mDigest = MessageDigest.getInstance("SHA1");
        byte[] result = mDigest.digest(input.getBytes());
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < result.length; i++) {
            sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
        }

        return sb.toString();
    }

}
