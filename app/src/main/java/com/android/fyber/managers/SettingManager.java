package com.android.fyber.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.android.fyber.FyberApplication;

/**
 * Created by manyeon on 11/5/15.
 */
public class SettingManager {

    private static SharedPreferences mSharedPreferences = null;

    public static final String PREF_ADVERT_ID = "pref_advert_id";
    public static final String PREF_ADVERT_LIMITED_TRACKING_ENABLED = "pref_advert_limited_tracking_enabled";
    public static final String PREF_SHA1 = "pref_sha1";

    private Context mContext;

    public SettingManager(Context pContext) {
        mContext = pContext;
    }

    public SharedPreferences getSharedPreferences() {
        if(mSharedPreferences == null) {
            mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        }

        return mSharedPreferences;
    }

    public void setAdvertId(String pValue) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(PREF_ADVERT_ID, pValue);
        editor.commit();
    }

    public String getAdvertId() {
        return getSharedPreferences().getString(PREF_ADVERT_ID, "N/A");
    }

    public void setAdvertLimitedTracking(boolean pValue) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putBoolean(PREF_ADVERT_LIMITED_TRACKING_ENABLED, pValue);
        editor.commit();
    }

    public boolean getAdvertLimitedTracking() {
        return getSharedPreferences().getBoolean(PREF_ADVERT_LIMITED_TRACKING_ENABLED, false);
    }

    public void setSha1(String pValue) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(PREF_SHA1, pValue);
        editor.commit();
    }

    public String getSha1() {
        return getSharedPreferences().getString(PREF_SHA1, "N/A");
    }
}
