package com.android.fyber.test;

import com.android.fyber.FyberApplication;
import com.android.fyber.managers.NetworkManager;
import com.android.fyber.managers.SettingManager;

import junit.framework.TestCase;

/**
 * Created by manyeon on 11/6/15.
 */
public class BasicTest extends TestCase {

    private FyberApplication mApp;
    private NetworkManager mNetworkManager;
    private SettingManager mSettingmanager;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mApp = FyberApplication.getApplication();
        mNetworkManager = mApp.getNetworkManager();
        mSettingmanager = mApp.getSettingManager();
    }

    public void testIPAddress() {
        assertNotNull(mApp.getUtilManager().getIPAddress());
    }

    public void testDeviceId() {
        assertNotSame("N/A", mApp.getUtilManager().getDeviceId());
    }

    public void testCheckSignature() {
       assertEquals(false, mApp.getCheckSignature());
    }
}
