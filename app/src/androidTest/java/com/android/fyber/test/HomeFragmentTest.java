package com.android.fyber.test;

import android.test.ActivityInstrumentationTestCase2;
import android.test.TouchUtils;
import android.test.ViewAsserts;
import android.test.suitebuilder.annotation.SmallTest;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.fyber.FyberApplication;
import com.android.fyber.R;
import com.android.fyber.activities.HomeActivity;
import com.android.fyber.fragments.HomeFragment;
import com.android.fyber.managers.NetworkManager;


/**
 * Created by manyeon on 11/6/15.
 */
public class HomeFragmentTest extends ActivityInstrumentationTestCase2<HomeActivity> {

    private NetworkManager mNetworkManager;
    private HomeActivity mActivity;
    private HomeFragment mFragment;
    private TextView mTitleMessage;
    private TextView mErrorMessage;
    private Button mRequestAPIButton;
    private RadioGroup mRadioAppID;
    private RadioGroup mRadioLocale;
    private RadioButton mPub2;

    public HomeFragmentTest() {
        super(HomeActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mNetworkManager = FyberApplication.getApplication().getNetworkManager();
        mActivity = (HomeActivity)getActivity();
        mFragment = mActivity.mFragment;
        mTitleMessage = (TextView)mFragment.getView().findViewById(R.id.titleMessage);
        mErrorMessage = (TextView)mFragment.getView().findViewById(R.id.errorMessage);
        mRequestAPIButton = (Button)mFragment.getView().findViewById(R.id.requestButton);
        mRadioAppID = (RadioGroup)mFragment.getView().findViewById(R.id.radioAppID);
        mRadioLocale = (RadioGroup)mFragment.getView().findViewById(R.id.radioLocale);
        mPub2 = (RadioButton)mFragment.getView().findViewById(R.id.pub2);
    }

    @SmallTest
    public void testPreConditions() {
        assertNotNull(mActivity);
        assertNotNull(mFragment);
        assertNotNull(mTitleMessage);
        assertNotNull(mErrorMessage);
        assertNotNull(mRequestAPIButton);
        assertNotNull(mRadioAppID);
        assertNotNull(mRadioLocale);
        assertNotNull(mPub2);

        getInstrumentation().waitForIdleSync();
    }

    @SmallTest
    public void testUI() {
        assertEquals("Please select param variable you want", mTitleMessage.getText().toString());
        assertNotSame("error", mErrorMessage.getText().toString());
        assertEquals("Request API", mRequestAPIButton.getText().toString());
        assertEquals(2, mRadioAppID.getChildCount());
        assertEquals(3, mRadioLocale.getChildCount());

        View decorView = mActivity.getWindow().getDecorView();
        ViewAsserts.assertOnScreen(decorView, mRequestAPIButton);
    }

    @SmallTest
    public void testLocaleRadioButton() {
        TouchUtils.clickView(this, mPub2);
        assertEquals("campaign2", mPub2.getText().toString());
    }

    @SmallTest
    public void testRequestButton() throws Throwable{

        if(mNetworkManager.isNetworkAvaiable(FyberApplication.getApplication().getApplicationContext()) == false) {
            TouchUtils.clickView(this, mRequestAPIButton);
            assertEquals("No network connection", mErrorMessage.getText().toString());
        }

    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
}
